-- Code belongs to TI3 DT
--
-- VCS: git@bitbucket.org/schaefers/DT-DEMO-SimpleGates.git
-- PUB: https://users.informatik.haw-hamburg.de/~schafers/PUBLIC/EXAMPLEs/VHDL/exampleSimpleGates/...
--
--
-- History:
-- ========
-- 111005 / WS11/12 :
--      1st version for TI3 DT WS11/12  by Michael Schaefers
-- WS14/15 (141104):
--      update for TI3 DT WS14/15  by Michael Schaefers
------------------------------------------------------------------------------
-- BEMERKUNG:
-- ==========
-- Deutscher Kommentar fuer Erklaerungen fuer speziell Studenten/VHDL-Anfaenger - dieser Kommentar wuerde im "Normalfall" fehlen
-- Englischer Kommentar als "normaler" Kommentar



library work;
    use work.all;                                           -- Zugriff auf Arbeitsbibliothek ("dorthin", wohin alles compiliert wird)

library ieee;
    use ieee.std_logic_1164.all;                            -- benoetigt fuer std_logic





-- Bemerkung(en):
-- ~~~~~~~~~~~~~~
--
-- Dies ist nur Beispiel-Code, der als einfache VHDL-Source-Code-Demo gelten soll.
-- Die "algorithmische Funktion" ist bewusst trivial, damit sich Studenten auf VHDL konzentrieren koennen.
-- Leider ist in der Konsequenz eine sinnvolle Benamung vereinzelt schwierig.
-- In einem "ernsthaften" Projekt sind Gatter KEINE ENTITYs.
-- 
-- Das folgende "delayVisualization"-GENERIC ist NUR fuer Anfaenger (um moegliche Probleme mit Delta-Zyklen zu entschaerfen).
-- Vor der Synthese sind codierte DELAYs schlechter Stil.
-- DELAYs werden automatisch bei der Bachannotation eingef�hrt.
-- Es gilt die Regel: Ein RTL-Designer schreibt KEIN "after" (im RTL-VHDL-Code).
-- Von daher kann nachfolgend generic(...); gern weggelassen werden ;-)
-- Wenn die GENERICs (fuer delayVisualization )weggelassen werden, dann empfiehlt es sich, dies ueberall zu tun.
entity ParityChecker is
    generic (
        delayVisualization : time := 0 ns                   -- visualization of just some HW delay
    );--]generic
    port (
        isEven  : out std_logic;                                 -- ACHTUNG: "o" ist eigentlich ein schlechter Name -> zu wenig Information im Namen
        i0 : in  std_logic;
        i1 : in  std_logic;
	i2 : in  std_logic;
	i3 : in  std_logic
    );--]port
end entity ParityChecker;

architecture arc of ParityChecker is                                 -- ACHTUNG: "arc" ist eigentlich ein schlechter Name -> bessere Namen sind z.B. behavior, rtl, structure, ...
begin
    
    DUT: 
    process (i0, i1, i2, i3 ) is
    begin
	isEven  <= NOT (i0 XOR i1 XOR i2 XOR i3);         -- NUR f�r VHDL-Anfaenger -> RTL-Designer schreiben nur: "o  <=  i1 and i2;"
    end process DUT;
        
end architecture arc;


library ieee;
    use ieee.std_logic_1164.all;                            -- benoetigt fuer std_logic

entity SG4PC is
    generic (
        delayVisualization : time := 0 ns                   -- visualization of just some HW delay
    );--]generic
	--port map(isEven, i0_tb, i1, i2, i3)
    port (
        out0  : out std_logic;                                 -- ACHTUNG: "o" ist eigentlich ein schlechter Name -> zu wenig Information im Namen
	out1  : out std_logic;
	out2  : out std_logic;
	out3  : out std_logic	
    );--]port
end entity SG4PC;

architecture test of SG4PC is  
begin
	sdiuf: 
  	  process is
  	  begin
	out0 <= '1'; out1 <= '1'; out2 <= '1'; out3 <= '1'; wait for 10 ns;
	out0 <= '1'; out1 <= '1'; out2 <= '1'; out3 <= '0'; wait for 10 ns;
	out0 <= '1'; out1 <= '1'; out2 <= '0'; out3 <= '1'; wait for 10 ns;
	out0 <= '1'; out1 <= '1'; out2 <= '0'; out3 <= '0'; wait for 10 ns;
	out0 <= '1'; out1 <= '0'; out2 <= '1'; out3 <= '1'; wait for 10 ns;
	out0 <= '1'; out1 <= '0'; out2 <= '1'; out3 <= '0'; wait for 10 ns;
	out0 <= '1'; out1 <= '0'; out2 <= '0'; out3 <= '1'; wait for 10 ns;
	out0 <= '1'; out1 <= '0'; out2 <= '0'; out3 <= '0'; wait for 10 ns;
	out0 <= '0'; out1 <= '1'; out2 <= '1'; out3 <= '1'; wait for 10 ns;
	out0 <= '0'; out1 <= '1'; out2 <= '1'; out3 <= '0'; wait for 10 ns;
	out0 <= '0'; out1 <= '1'; out2 <= '0'; out3 <= '1'; wait for 10 ns;
	out0 <= '0'; out1 <= '1'; out2 <= '0'; out3 <= '0'; wait for 10 ns;
	out0 <= '0'; out1 <= '0'; out2 <= '1'; out3 <= '1'; wait for 10 ns;
	out0 <= '0'; out1 <= '0'; out2 <= '1'; out3 <= '0'; wait for 10 ns;
	out0 <= '0'; out1 <= '0'; out2 <= '0'; out3 <= '1'; wait for 10 ns;
	out0 <= '0'; out1 <= '0'; out2 <= '0'; out3 <= '0'; wait for 10 ns;
		wait;         -- NUR f�r VHDL-Anfaenger -> RTL-Designer schreiben nur: "o  <=  i1 and i2;"
    	end process sdiuf;
end architecture test;

library ieee;
    use ieee.std_logic_1164.all;                            -- benoetigt fuer std_logic

entity TB4PC is   
end entity TB4PC;

architecture arc of TB4PC is                                 -- ACHTUNG: "arc" ist eigentlich ein schlechter Name -> bessere Namen sind z.B. behavior, rtl, structure, ...

	signal bit0 : std_logic;
	signal bit1 : std_logic;
	signal bit2 : std_logic;
	signal bit3 : std_logic;

	signal result_e : std_logic;

	component SG is 
	port(
		out0 : out std_logic;
		out1 : out std_logic;
		out2 : out std_logic;
		out3 : out std_logic
	);
	end component  SG;
	for all : SG use entity work.SG4PC;

	component PC is 
	port(
		isEven : out std_logic;
		i0 : in std_logic;
		i1 : in std_logic;
		i2 : in std_logic;
		i3 : in std_logic
	);
	end component  PC;
	for all : PC use entity work.ParityChecker;
begin
	sg_i : SG port map(out0 => bit0, out1 => bit1, out2 => bit2, out3 => bit3);
	pc_1 : PC port map(i0 => bit0, i1 => bit1, i2 => bit2, i3 => bit3, isEven => result_e);
end architecture arc;

